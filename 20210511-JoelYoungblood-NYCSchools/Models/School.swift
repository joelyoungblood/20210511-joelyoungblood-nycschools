//
//  School.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import Foundation
import CoreLocation

struct School: Codable {
    let id: String
    let name: String
    let overview: String
    let phone: String
    let website: String
    let extracurricularActivities: String?
    let address: String
    let city: String
    let zip: String
    let state: String
    
    private enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case overview = "overview_paragraph"
        case phone = "phone_number"
        case website
        case extracurricularActivities = "extracurricular_activities"
        case address = "primary_address_line_1"
        case city, zip
        case state = "state_code"
    }
}
