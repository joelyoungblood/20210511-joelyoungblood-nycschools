//
//  SatStats.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import Foundation

struct SchoolSATData: Codable {
    let totalTestTakers: Int
    let criticalReadingAverage: Double
    let mathAverage: Double
    let writingAverage: Double
    
    private enum CodingKeys: String, CodingKey {
        case totalTestTakers = "num_of_sat_test_takers"
        case criticalReadingAverage = "sat_critical_reading_avg_score"
        case mathAverage = "sat_math_avg_score"
        case writingAverage = "sat_writing_avg_score"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let totalString = try container.decode(String.self, forKey: .totalTestTakers)
        totalTestTakers = Int(totalString) ?? 0
        let readingAverage = try container.decode(String.self, forKey: .criticalReadingAverage)
        criticalReadingAverage = Double(readingAverage) ?? 0.0
        let math = try container.decode(String.self, forKey: .mathAverage)
        mathAverage = Double(math) ?? 0.0
        let writing = try container.decode(String.self, forKey: .writingAverage)
        writingAverage = Double(writing) ?? 0.0
    }
}
