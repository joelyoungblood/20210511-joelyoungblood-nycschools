//
//  Network.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import Foundation
import Combine

struct Network {
    //You may notice that networking is done through Combine, while everything else is done through Rx. There is a simple reason: Combine has great built in support for URLSession, and specifically, decoding a generic type, and ultimately type erasing the subscriber type. You'd have to write quite alot more code to do that in Rx. However, Combine lags way way way behind Rx in terms of integration with UIKit / Cocoa in general. I suspect Combine will either get there...or stay more SwiftUI focused. Who can tell...?
    static func request<T: Decodable>( _ api: ApiRoute, forType type: T.Type) -> AnyPublisher<T, Error> {
        URLSession.shared
            .dataTaskPublisher(for: api.asURLRequest())
            .map(\.data)
            .decode(type: T.self, decoder: JSONDecoder())
            .share()
            .eraseToAnyPublisher()
    }
}
