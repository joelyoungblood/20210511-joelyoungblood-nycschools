//
//  SchoolsAPI.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import Foundation

//By having individual api 'routes' that confirm to ApiRoute, it makes it much much easier to organize your network calls
enum SchoolsAPI: ApiRoute {
    case allSchools
    case testResultsForSchoolWithId(id: String)
    
    var path: String {
        switch self {
        case .allSchools:
            return "resource/s3k6-pzi2.json"
        case .testResultsForSchoolWithId(let id):
            return "resource/f9bf-2cp4.json?dbn=\(id)"
        }
    }
    
    var params: JSON? {
        return nil
    }
}
