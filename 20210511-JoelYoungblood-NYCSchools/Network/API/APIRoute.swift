//
//  APIRoute.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import Foundation

// Syntactic sugar ;)
typealias JSON = [String: Any]

protocol ApiRoute {
    var root: String { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var params: JSON? { get }
}

extension ApiRoute {
    //I go through this step so that we could switch our root url depending on the environment, eg. prod, dev, test etc, as well as it's nice to have the option if differant apis have differant roots
    var root: String {
        return Constants.Network.apiBaseUrl
    }
    
    // Can obviously be overriden by anything conforming to ApiRoute, but we only need get for this example
    var method: HTTPMethod {
        return .get
    }
    
    // This is my preferred spot to grab a request and add any auth headers if I need
    func asURLRequest() -> URLRequest {
        let url = URL(string: root + path)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return urlRequest
    }
}
