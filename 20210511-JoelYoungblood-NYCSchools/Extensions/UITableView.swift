//
//  UITableView.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/11/21.
//

import UIKit

//I originally got all this from a ray wenderlich article...ooof, five, six years ago? I've kept it up to date and it's never failed me, and as you can see in the view controllers, does make everything more convenient!
extension UITableView {
    
    func register(_ cell: AnyClass) {
        self.register(cell.self, forCellReuseIdentifier: String(describing: cell))
    }
    
    func registerHeaderFooter(_ view: AnyClass) {
        self.register(view.self, forHeaderFooterViewReuseIdentifier: String(describing: view))
    }
    
    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
    
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T where T: ReusableView {
        guard let view = self.dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T else {
            fatalError("Could not dequeue UITableViewHeaderFooterView with identifier: \(T.reuseIdentifier)")
        }
        return view
    }
    
}

protocol ReusableView: class { }

extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
