//
//  Collection.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import Foundation

extension Collection {
    //Always be safe! Given swift was made with optionals in mind, I always thought it was odd this wasn't included in Foundation...
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
