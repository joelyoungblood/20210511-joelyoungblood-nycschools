//
//  SchoolDetailViewController.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import UIKit
import RxSwift
import RxCocoa

final class SchoolDetailViewController: UIViewController {
    private let disposeBag = DisposeBag()
    private let viewModel: SchoolDetailViewModel
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.alwaysBounceVertical = false
        tableView.estimatedRowHeight = 120.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        tableView.register(SchoolDetailHeaderTableViewCell.self)
        tableView.register(SATScoresHeaderTableViewCell.self)
        tableView.register(NoSATDataTableViewCell.self)
        tableView.register(SchoolDetailOverviewTableViewCell.self)
        tableView.register(ExtracurricularActivitiesAvailableTableViewCell.self)
        tableView.register(ContactTableViewCell.self)
        tableView.register(ScoreDataTableViewCell.self)
        return tableView
    }()
    
    init(withSchool school: School) {
        viewModel = SchoolDetailViewModel(withSchool: school)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        bindObservables()
    }
    
    private func bindObservables() {
        viewModel
            .currentSections
            .bind(to: tableView.rx.items(dataSource: viewModel.dataSource))
            .disposed(by: disposeBag)
        
        viewModel.fetchTestingData()
    }
}
