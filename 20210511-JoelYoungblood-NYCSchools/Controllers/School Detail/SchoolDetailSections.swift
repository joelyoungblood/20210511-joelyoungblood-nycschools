//
//  SchoolDetailSections.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import Foundation
import RxDataSources

//All these enums are used by RxDataSources to 'make the magic happen' so to speak. If you're curious see https://github.com/RxSwiftCommunity/RxDataSources
enum SchoolDetailSections {
    case schoolName(items: [SchoolDetailItem])
    case noScores(items: [SchoolDetailItem])
    case scores(items: [SchoolDetailItem])
    case additionalDetails(items: [SchoolDetailItem])
    case contact(items: [SchoolDetailItem])
}

extension SchoolDetailSections: SectionModelType {
    typealias Item = SchoolDetailItem
    
    var items: [SchoolDetailItem] {
        switch self {
        case .schoolName(let items):
            return items
        case .noScores(let items):
            return items
        case .scores(let items):
            return items
        case .additionalDetails(let items):
            return items
        case .contact(let items):
            return items
        }
    }
    
    init(original: SchoolDetailSections, items: [SchoolDetailItem]) {
        switch original {
        case .schoolName(let items):
            self = .schoolName(items: items)
        case .noScores(let items):
            self = .noScores(items: items)
        case .scores(let items):
            self = .scores(items: items)
        case .additionalDetails(let items):
            self = .additionalDetails(items: items)
        case .contact(let items):
            self = .contact(items: items)
        }
    }
}

enum SchoolDetailItem {
    case schoolName
    case scoreHeader
    case scoreRow(title: String, data: Double)
    case noScoreData
    case overview
    case extracurricularActivities
    case contact
}
