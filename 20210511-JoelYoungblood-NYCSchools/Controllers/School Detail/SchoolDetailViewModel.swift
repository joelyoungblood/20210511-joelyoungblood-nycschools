//
//  SchooDetailViewModel.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import Foundation
import Combine
import RxSwift
import RxDataSources

final class SchoolDetailViewModel {
    private let disposeBag = DisposeBag()
    private var cancellables = Set<AnyCancellable>()
    private let school: School
    let currentSections = PublishSubject<[SchoolDetailSections]>()
    
    init(withSchool selectedSchool: School) {
        school = selectedSchool
    }
    
    lazy var dataSource = RxTableViewSectionedReloadDataSource<SchoolDetailSections>(configureCell: { [weak self] data, tableView, indexPath, _ -> UITableViewCell in
        guard let `self` = self else { return UITableViewCell() }
        switch data[indexPath] {
        case .schoolName:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as SchoolDetailHeaderTableViewCell
            cell.configure(with: self.school.name)
            return cell
        case .scoreHeader:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as SATScoresHeaderTableViewCell
            cell.configure(withSchoolName: self.school.name)
            return cell
        case .scoreRow(let title, let data):
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ScoreDataTableViewCell
            cell.configure(forStat: title, andData: data)
            return cell
        case .noScoreData:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as NoSATDataTableViewCell
            cell.configure(with: self.school)
            return cell
        case .overview:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as SchoolDetailOverviewTableViewCell
            cell.configure(with: self.school)
            return cell
        case .extracurricularActivities:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ExtracurricularActivitiesAvailableTableViewCell
            cell.configure(withActivities: self.school.extracurricularActivities)
            return cell
        case .contact:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ContactTableViewCell
            cell.configure(with: self.school)
            return cell
        }
    })
    
    func fetchTestingData() {
        Network.request(SchoolsAPI.testResultsForSchoolWithId(id: school.id), forType: [SchoolSATData].self)
            .receive(on: DispatchQueue.main)
            .sink { error in
                print("Print this is the 'error' \(error)")
            } receiveValue: { [weak self] stats in
                self?.buildSections(withStats: stats)
            }.store(in: &cancellables)
    }
    
    //Building up the sections of a multi-cell tableview with Rx is more time consuming, but ultimately adaptable enough I find it well worth it
    private func buildSections(withStats stats: [SchoolSATData]) {
        var sections: [SchoolDetailSections] = [.schoolName(items: [.schoolName])]
        if let currentStats = stats.first {
            sections += [.scores(items: [
                .scoreHeader,
                .scoreRow(title: "Total Test Takers", data: Double(currentStats.totalTestTakers)),
                .scoreRow(title: "Critical Reading Average", data: currentStats.criticalReadingAverage),
                .scoreRow(title: "Math Average", data: currentStats.mathAverage),
                .scoreRow(title: "Writing Average", data: currentStats.writingAverage)
            ])]
        } else {
            sections += [.noScores(items: [.noScoreData])]
        }
        if let extraActivities = school.extracurricularActivities {
            if extraActivities.count > 0 {
                sections += [.additionalDetails(items: [.overview, .extracurricularActivities])]
            } else {
                sections += [.additionalDetails(items: [.overview])]
            }
        } else {
            sections += [.additionalDetails(items: [.overview])]

        }
        sections += [.contact(items: [.contact])]
        currentSections.onNext(sections)
    }
}
