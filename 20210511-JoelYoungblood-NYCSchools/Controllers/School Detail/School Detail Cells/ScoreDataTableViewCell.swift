//
//  ScoresTableViewCell.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/11/21.
//

import UIKit

final class ScoreDataTableViewCell: UITableViewCell, ReusableView {
    
    private let statisticTypeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = .systemFont(ofSize: 16.0, weight: .light)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let statisticDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = .systemFont(ofSize: 16.0, weight: .regular)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let divider: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let specialDataOffset: CGFloat = 75.0
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    private func commonInit() {
        selectionStyle = .none
        
        contentView.addSubview(divider)
        NSLayoutConstraint.activate([
            divider.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Constants.UI.defaultHorizontalOffset),
            divider.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -specialDataOffset),
            divider.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor),
            divider.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
        contentView.addSubview(statisticTypeLabel)
        NSLayoutConstraint.activate([
            statisticTypeLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Constants.UI.defaultHorizontalOffset),
            statisticTypeLabel.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: Constants.UI.defaultVerticalOffset),
            statisticTypeLabel.bottomAnchor.constraint(equalTo: divider.safeAreaLayoutGuide.bottomAnchor, constant: -Constants.UI.defaultVerticalOffset)
        ])
        
        contentView.addSubview(statisticDataLabel)
        NSLayoutConstraint.activate([
            statisticDataLabel.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -specialDataOffset),
            statisticDataLabel.centerYAnchor.constraint(equalTo: statisticTypeLabel.safeAreaLayoutGuide.centerYAnchor)
        ])
    }
    
    func configure(forStat type: String, andData data: Double) {
        statisticTypeLabel.text = type
        // Ideally we'd employ a number formatter here to ensure whatever numbers we get back will always display the same...but time is a factor for me here ;)
        statisticDataLabel.text = "\(data)"
    }
}
