//
//  ContactTableViewCell.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/11/21.
//

import UIKit
import RxSwift
import RxCocoa

final class ContactTableViewCell: UITableViewCell, ReusableView {
    var disposeBag = DisposeBag()
    
    private let websiteLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12.0, weight: .thin)
        label.textColor = .darkGray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Website:"
        return label
    }()
    
    //both this and the phone button were supposed to appropriately direct you to the web or phone, but time ran out
    private let websiteButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.systemBlue, for: [])
        button.titleLabel?.font = .systemFont(ofSize: 12.0)
        button.titleLabel?.textAlignment = .left
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let phoneButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.systemBlue, for: [])
        button.titleLabel?.font = .systemFont(ofSize: 12.0)
        button.titleLabel?.textAlignment = .left
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let phoneLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12.0, weight: .thin)
        label.textColor = .darkGray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Phone:"
        return label
    }()
    
    private let addressHeaderLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12.0, weight: .thin)
        label.textColor = .darkGray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Address:"
        return label
    }()
    
    private let addressLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14.0, weight: .thin)
        label.textColor = .darkGray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    private func commonInit() {
        selectionStyle = .none
        
        contentView.addSubview(phoneLabel)
        NSLayoutConstraint.activate([
            phoneLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Constants.UI.defaultHorizontalOffset),
            phoneLabel.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: Constants.UI.defaultVerticalOffset)
        ])
        
        contentView.addSubview(phoneButton)
        NSLayoutConstraint.activate([
            phoneButton.leadingAnchor.constraint(equalTo: phoneLabel.safeAreaLayoutGuide.leadingAnchor),
            phoneButton.topAnchor.constraint(equalTo: phoneLabel.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        contentView.addSubview(websiteLabel)
        NSLayoutConstraint.activate([
            websiteLabel.topAnchor.constraint(equalTo: phoneLabel.safeAreaLayoutGuide.topAnchor),
            websiteLabel.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -Constants.UI.defaultHorizontalOffset)
        ])
        
        contentView.addSubview(websiteButton)
        NSLayoutConstraint.activate([
            websiteButton.topAnchor.constraint(equalTo: websiteLabel.safeAreaLayoutGuide.bottomAnchor),
            websiteButton.trailingAnchor.constraint(equalTo: websiteLabel.safeAreaLayoutGuide.trailingAnchor),
        ])
        
        contentView.addSubview(addressHeaderLabel)
        NSLayoutConstraint.activate([
            addressHeaderLabel.leadingAnchor.constraint(equalTo: phoneLabel.safeAreaLayoutGuide.leadingAnchor),
            addressHeaderLabel.topAnchor.constraint(equalTo: phoneButton.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        contentView.addSubview(addressLabel)
        NSLayoutConstraint.activate([
            addressLabel.leadingAnchor.constraint(equalTo: addressHeaderLabel.safeAreaLayoutGuide.leadingAnchor),
            addressLabel.topAnchor.constraint(equalTo: addressHeaderLabel.safeAreaLayoutGuide.bottomAnchor),
            addressLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -Constants.UI.defaultHorizontalOffset),
            addressLabel.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: -Constants.UI.defaultVerticalOffset)
        ])
    }
    
    func configure(with school: School) {
        websiteButton.setTitle(school.website, for: [])
        phoneButton.setTitle(school.phone, for: [])
        /**
             let address: String
             let city: String
             let zip: String
             let state: String
             */
        addressLabel.text = "\(school.address)\nCity: \(school.city)\nState: \(school.state) \(school.zip)"
    }
}
