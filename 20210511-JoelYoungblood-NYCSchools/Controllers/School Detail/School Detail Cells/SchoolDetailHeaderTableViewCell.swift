//
//  GenericHeaderTableViewCell.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import UIKit

final class SchoolDetailHeaderTableViewCell: UITableViewCell, ReusableView {
    private let headerLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 30.0, weight: .regular)
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dividerView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    private func commonInit() {
        selectionStyle = .none
        
        contentView.addSubview(dividerView)
        NSLayoutConstraint.activate([
            dividerView.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Constants.UI.defaultHorizontalOffset),
            dividerView.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor),
            dividerView.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor),
            dividerView.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
        contentView.addSubview(headerLabel)
        NSLayoutConstraint.activate([
            headerLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Constants.UI.defaultHorizontalOffset),
            headerLabel.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -Constants.UI.defaultHorizontalOffset),
            headerLabel.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: Constants.UI.defaultVerticalOffset),
            headerLabel.bottomAnchor.constraint(lessThanOrEqualTo: dividerView.safeAreaLayoutGuide.topAnchor, constant: -Constants.UI.defaultVerticalOffset)
        ])
    }
    
    func configure(with name: String) {
        headerLabel.text = name
    }
}
