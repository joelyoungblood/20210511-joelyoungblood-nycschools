//
//  NoSATDataTableViewCell.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/11/21.
//

import UIKit

final class NoSATDataTableViewCell: UITableViewCell, ReusableView {
    private let errorImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "exclamationmark.bubble")
        imageView.tintColor = .systemRed
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let errorLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16.0, weight: .heavy)
        label.textColor = .darkGray
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dividerView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    private func commonInit() {
        selectionStyle = .none
        
        contentView.addSubview(errorImageView)
        NSLayoutConstraint.activate([
            errorImageView.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: Constants.UI.defaultHorizontalOffset),
            errorImageView.centerXAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.centerXAnchor),
            errorImageView.widthAnchor.constraint(equalToConstant: 60.0),
            errorImageView.heightAnchor.constraint(equalToConstant: 60.0)
        ])
        
        contentView.addSubview(dividerView)
        NSLayoutConstraint.activate([
            dividerView.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Constants.UI.defaultHorizontalOffset),
            dividerView.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor),
            dividerView.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor),
            dividerView.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
        contentView.addSubview(errorLabel)
        NSLayoutConstraint.activate([
            errorLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Constants.UI.defaultHorizontalOffset),
            errorLabel.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -Constants.UI.defaultHorizontalOffset),
            errorLabel.topAnchor.constraint(equalTo: errorImageView.safeAreaLayoutGuide.bottomAnchor, constant: Constants.UI.defaultVerticalOffset),
            errorLabel.bottomAnchor.constraint(equalTo: dividerView.safeAreaLayoutGuide.topAnchor, constant: -Constants.UI.defaultVerticalOffset)
        ])
    }
    
    func configure(with school: School) {
        errorLabel.text = "We're sorry! We weren't able to find any data relating to recent SAT testing for \(school.name)"
    }
}
