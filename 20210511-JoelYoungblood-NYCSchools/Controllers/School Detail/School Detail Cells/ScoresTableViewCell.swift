//
//  ScoresTableViewCell.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/11/21.
//

import UIKit

final class ScoreDataTableViewCell: UITableViewCell, ReusableView {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    private func commonInit() {
        selectionStyle = .none
    }
}
