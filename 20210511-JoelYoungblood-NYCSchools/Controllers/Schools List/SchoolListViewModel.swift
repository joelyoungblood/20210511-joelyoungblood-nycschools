//
//  SchoolListViewModel.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import UIKit
import Combine
import RxSwift
import RxDataSources

struct AllSchoolsSectionData {
    var items: [School]
}

extension AllSchoolsSectionData: SectionModelType {
    typealias Item = School
    
    init(original: Self, items: [Self.Item]) {
        self = original
        self.items = items
    }
}

final class SchoolListViewModel {
    private var subscriptions = Set<AnyCancellable>()
    let schools = PublishSubject<[School]>()
    let onError = PublishSubject<Void>()
    let isLoading = PublishSubject<Bool>()
    
    //I like to have as little code as possible in my view controllers, and find that generally this property is better built in the view model anyway
    lazy var dataSource = RxTableViewSectionedReloadDataSource<AllSchoolsSectionData>(configureCell: { _, tableView, indexPath, school -> UITableViewCell in
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as SchoolListViewTableViewCell

        cell.websiteButton.rx.tap.subscribe(onNext: {
            if let url = URL(string: school.website) {
                //You can clearly see the intention here - however, I kept getting strange errors with testing. Could be sim related? I would need more time to determine, not to mention these aren't all properly formed URLs...
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }).disposed(by: cell.disposeBag)
        
        cell.configure(for: school)
        return cell
    })
    
    func fetchAllSchools() {
        isLoading.onNext(true)
        Network.request(SchoolsAPI.allSchools, forType: [School].self).receive(on: DispatchQueue.main).sink { [weak self] _ in
            self?.isLoading.onNext(false)
        } receiveValue: { [weak self] schoolResults in
            self?.isLoading.onNext(false)
            self?.schools.onNext(schoolResults)
        }
        .store(in: &subscriptions)
    }
}
