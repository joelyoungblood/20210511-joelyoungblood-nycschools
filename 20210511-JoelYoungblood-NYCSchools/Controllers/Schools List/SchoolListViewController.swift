//
//  SchoolListViewController.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import UIKit
import RxSwift
import RxDataSources

final class SchoolListViewController: UIViewController {
    private let disposeBag = DisposeBag()
    private let viewModel = SchoolListViewModel()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorColor = .darkGray
        tableView.estimatedRowHeight = 600
        tableView.rowHeight = UITableView.automaticDimension
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(SchoolListViewTableViewCell.self)
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        tableView.addSubview(refreshControl)
        
        bindObservables()
    }
    
    // The terminology of 'binding' is fairly common in RxSwift - you frequently (as with these table views) 'bind' two observables together, so this method call terminology is fairly standardized
    private func bindObservables() {
        refreshControl.rx
            .controlEvent(.valueChanged)
            .subscribe(onNext: { [weak self] in
                self?.viewModel.fetchAllSchools()
            }).disposed(by: disposeBag)
        
        viewModel.isLoading
            .subscribe(onNext: { [weak self] loading in
                loading ? self?.refreshControl.beginRefreshing() : self?.refreshControl.endRefreshing()
            }).disposed(by: disposeBag)
        
        viewModel.schools
            .map { [AllSchoolsSectionData(items: $0)] }
            .bind(to: tableView.rx.items(dataSource: viewModel.dataSource))
            .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(School.self).subscribeOn(MainScheduler.instance).subscribe(onNext: { [weak self] school in
            self?.navigationItem.backButtonTitle = ""
            self?.navigationController?.pushViewController(SchoolDetailViewController(withSchool: school), animated: true)
        }).disposed(by: disposeBag)
        
        //we make the actual network call last, just to be sure that all of the subscribers for the call are actually set up and listening!
        viewModel.fetchAllSchools()
    }
}
