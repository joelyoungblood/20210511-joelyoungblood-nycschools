//
//  SchoolListViewTableViewCell.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import UIKit
import RxSwift

final class SchoolListViewTableViewCell: UITableViewCell, ReusableView {
    var disposeBag = DisposeBag()
    //I prefer to use this process of initialization where possible. In my opinion it makes the code more readable, it's easier to jump to a specific property that may be the issue, and it avoids filling up initializer methods with more UI code than is necessary
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 24.0, weight: .semibold)
        label.textColor = .black
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let websiteButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.systemBlue, for: [])
        button.titleLabel?.font = .systemFont(ofSize: 12.0)
        button.titleLabel?.textAlignment = .left
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14.0, weight: .thin)
        label.textColor = .darkGray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //Rx has the potential to cause memory issues when unmanaged in tv/cv cells. This is the generally accepted way of handling any retain issues
        disposeBag = DisposeBag()
    }
    
    // I've used this commonInit method on my tableView / collectionView cells ever since learning it was how a certain large firm with a capital 'G' in its name does it. I have no regrets and wouldn't go back, especially since I do all my layout in code, as you can see
    private func commonInit() {
        selectionStyle = .none
        accessoryType = .disclosureIndicator
        
        contentView.addSubview(nameLabel)
        NSLayoutConstraint.activate([
            nameLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Constants.UI.defaultHorizontalOffset),
            nameLabel.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: Constants.UI.defaultVerticalOffset),
            nameLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: Constants.UI.defaultHorizontalOffset)
        ])
        
        contentView.addSubview(websiteButton)
        NSLayoutConstraint.activate([
            websiteButton.leadingAnchor.constraint(equalTo: nameLabel.safeAreaLayoutGuide.leadingAnchor),
            websiteButton.topAnchor.constraint(equalTo: nameLabel.safeAreaLayoutGuide.bottomAnchor, constant: Constants.UI.intraItemOffset),
        ])
        
        contentView.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            descriptionLabel.leadingAnchor.constraint(equalTo: websiteButton.safeAreaLayoutGuide.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: nameLabel.safeAreaLayoutGuide.trailingAnchor),
            descriptionLabel.topAnchor.constraint(equalTo: websiteButton.safeAreaLayoutGuide.bottomAnchor, constant: Constants.UI.defaultVerticalOffset),
            descriptionLabel.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: -Constants.UI.defaultVerticalOffset)
        ])
    }
    
    func configure(for school: School) {
        nameLabel.text = school.name
        websiteButton.setTitle(school.website, for: [])
        descriptionLabel.text = school.overview
    }
}
