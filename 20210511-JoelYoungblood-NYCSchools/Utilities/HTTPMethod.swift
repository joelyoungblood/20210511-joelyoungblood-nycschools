//
//  HTTPMethod.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import Foundation

//Again, this is really just syntactic sugar for ApiRoute
public struct HTTPMethod: RawRepresentable, Equatable, Hashable {
    public static let delete = HTTPMethod(rawValue: "DELETE")
    public static let get = HTTPMethod(rawValue: "GET")
    public static let patch = HTTPMethod(rawValue: "PATCH")
    public static let post = HTTPMethod(rawValue: "POST")
    public static let put = HTTPMethod(rawValue: "PUT")

    public let rawValue: String

    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}
