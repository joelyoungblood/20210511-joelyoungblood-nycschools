//
//  SceneDelegate.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import UIKit
import SwiftUI

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        //It would be nice to have actually used some SwiftUI in here, but my brain hasn't fully transitioned from Flutter to SwiftUI, and the navigation in SwiftUI right now is highly...annoying
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = UINavigationController(rootViewController: SchoolListViewController())
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}

