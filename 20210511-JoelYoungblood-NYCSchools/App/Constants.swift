//
//  Constants.swift
//  20210511-JoelYoungblood-NYCSchools
//
//  Created by Joel Youngblood on 5/10/21.
//

import UIKit

//I always try and keep a high level Constants file like this, broken up into individual structs, because I cannot, and will not, abide literals in my code! Obviously this is overkill (and there are string literals in my code - everyone runs out of time...some times)
struct Constants {
    
    struct Network {
        static let apiBaseUrl = "https://data.cityofnewyork.us/"
    }
    
    struct UI {
        static let defaultHorizontalOffset: CGFloat = 15.0
        static let defaultVerticalOffset: CGFloat = 10.0
        static let intraItemOffset: CGFloat = 5.0
    }
}
